package training;

/* The test class or client */
public class PressSwitch {
   public static void main(final String[] arguments){
      // Check number of arguments
      if (arguments.length != 1) {
         System.err.println("Argument \"ON\" or \"OFF\" is required.");
         System.exit(-1);
      }

      // Invoker object(s), command objects and receiver objects are held by a client object, which is this class PressSwitch.
      //Invoker object = mySwitch
      //Command objects = switchUp and switchDown
      //Receiver objects = lamp
      
      final Light lamp = new Light();  
       
     //The client ( PressSwitch ) decides which receiver objects ( lamp ) it assigns to the command objects ( switchUp ) 
      final Command switchUp = new FlipUpCommand(lamp);
      
    //The client ( PressSwitch ) decides which receiver objects ( lamp ) it assigns to the command objects ( switchDown ) 
      final Command switchDown = new FlipDownCommand(lamp);
      
      
      final Switch mySwitch = new Switch();

      // The client ( PressSwitch ) decides which commands ( switchUp or switchDown ) it assigns to the invoker ( mySwitch ).
      switch(arguments[0]) {
         case "ON":
            mySwitch.storeAndExecute(switchUp);
            break;
         case "OFF":
            mySwitch.storeAndExecute(switchDown);
            break;
         default:
            System.err.println("Argument \"ON\" or \"OFF\" is required.");
            System.exit(-1);
      }
   }
}
