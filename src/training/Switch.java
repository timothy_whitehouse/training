package training;

import java.util.ArrayList;
import java.util.List;

/** The Invoker class */

public class Switch {
	   private List<Command> history = new ArrayList<Command>(); //The invoker class only about command interface.

	   public void storeAndExecute(final Command cmd) {

		  this.history.add(cmd);
		  
	      cmd.execute(); //An invoker object knows how to execute a command, does not know anything about a concrete command.
	      				 //( remember it , it knows only about command interface above )
	   }
	}