package training;

//A command object = FlipUpCommand knows about a receiver
	//receiver = theLight
//The command object invokes a method of the receiver
	//method from theLight = turnOff().

/** The Command for turning on the light - ConcreteCommand #1 */
public class FlipUpCommand implements Command {
	
	   private Light theLight; 

	   public FlipUpCommand(final Light light) {
		   
	      this.theLight = light; //the receiver object to execute these methods is also stored in the command object by aggregation
	   }

	   @Override    // execute() is from the interface "Command.java"
	   public void execute() {
		   
	      theLight.turnOn(); //The receiver then does the work when the execute() method in command is called
	   }
}
