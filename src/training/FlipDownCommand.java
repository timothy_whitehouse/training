package training;

//A command object = FlipDownCommand knows about a receiver
	//receiver = theLight
//The command object invokes a method of the receiver
	//method from theLight = turnOff().

/** The Command for turning off the light - ConcreteCommand #2 */
public class FlipDownCommand implements Command {
	
   private Light theLight; 

   public FlipDownCommand(final Light light) {
	   
      this.theLight = light;  //the receiver object to execute these methods is also stored in the command object by aggregation
   }

   
   @Override    // execute() is from the interface "Command.java"
   public void execute() {
	   
      theLight.turnOff(); //The receiver then does the work when the execute() method in command is called
   }
}

